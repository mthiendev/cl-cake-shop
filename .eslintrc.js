module.exports = {
  root: false,
  env: {
    node: true
  },
  extends: ['plugin:vue/essential', '@vue/prettier'],
  rules: {
    'no-console':  'off',
    'no-debugger': 'off',
    'max-len': ['error', { code: 360 }],
    'prettier/prettier': "off" // 
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
};
