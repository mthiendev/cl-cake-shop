import Swal from 'sweetalert2';

const SweetAlertPlugin = {
  install(Vue) {
    Vue.prototype.$swal = Swal;
  },
};

export default SweetAlertPlugin;
