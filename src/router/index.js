import Vue from 'vue';
import VueRouter from 'vue-router';
import HomeView from '@/views/pages/HomeView.vue';
import CartView from '@/views/pages/cart/CartView.vue';
import ProductList from '@/views/components/product/ProductList.vue';
import CartLayout from '@/views/pages/cart/CartLayout.vue';
import CartCheckout from '@/views/pages/cart/CartCheckout.vue';
import ThankyouPage from '@/views/pages/ThankyouPage.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    children: [
      {
        path: '/',
        name: 'home',
        component: ProductList,
      },
      {
        path: '/products',
        name: 'products',
        component: ProductList,
      },
      {
        path: '/cart',
        component: CartView,
        children: [
          {
            path: '/',
            name: 'cart',
            component: CartLayout,
          },
          {
            path: 'checkout',
            name: 'cart-checkout',
            component: CartCheckout,
          },
        ],
      },
      {
        path: '/thank-you',
        name: 'thank-you',
        component: ThankyouPage,
      },
      {
        path: '/about',
        name: 'about',
        component: () => import('@/views/pages/AboutView.vue'),
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
