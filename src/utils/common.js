import { BToast } from 'bootstrap-vue';

export function makeToastSuccess(message) {
  const bsToaster = new BToast();
  bsToaster.$bvToast.toast(message, {
    title: 'Noticate',
    autoHideDelay: 1000,
    variant: 'success',
    solid: true,
    appendToast: true,
  });
}

export function makeToastFaile(message) {
  const bsToaster = new BToast();
  bsToaster.$bvToast.toast(message, {
    title: 'Noticate',
    autoHideDelay: 1000,
    variant: 'danger',
    solid: true,
    appendToast: true,
  });
}
