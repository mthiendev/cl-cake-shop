export default {
  state: {
    items: [],
  },
  mutations: {
    setBreadcrumb(state, items) {
      state.items = items;
    },
  },
  actions: {
    updateBreadcrumb({ commit }, items) {
      commit('setBreadcrumb', items);
    },
  },
};
