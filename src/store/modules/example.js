const example = {
  state: {
    // State là nơi lưu trữ dữ liệu trong Vuex, tương tự như các biến trong một ứng dụng Vue.js thông thường.
    // State là một đối tượng chứa các thuộc tính dùng để lưu trữ các thông tin cần thiết cho ứng dụng của bạn.
    count: 0,
  },
  mutations: {
    // Mutations là các hàm được sử dụng để thay đổi (mutate) trạng thái trong Vuex.
    // Mutations được sử dụng để thực hiện các hành động cụ thể như thêm, xóa, cập nhật dữ liệu trong trạng thái.
    // Mutations phải là hàm "đồng bộ" (synchronous) và được gọi bằng cách sử dụng phương thức "commit".
    increment(state) {
      state.count++;
    },
  },
  actions: {
    // Actions là các hàm được sử dụng để thực hiện các tác vụ không đồng bộ (asynchronous) hoặc các tác vụ phức tạp trong Vuex.
    // Actions thường được sử dụng để gọi API, xử lý dữ liệu không đồng bộ hoặc thực hiện các tác vụ phức tạp trước khi thay đổi trạng thái bằng mutations.
    // Actions phải là hàm "bất đồng bộ" (asynchronous) và được gọi bằng cách sử dụng phương thức "dispatch".
    // Sau khi thực hiện xong các tác vụ, actions có thể gọi các mutations để thay đổi trạng thái.
    increment(context) {
      context.commit('increment');
    },
  },
  getters: {
    // Getters là các hàm được sử dụng để truy vấn dữ liệu từ trạng thái trong Vuex.
    // Getters giúp bạn lấy dữ liệu từ trạng thái mà không cần thay đổi nó.
    // Các getters có thể sử dụng để tính toán, lọc hoặc định dạng dữ liệu từ trạng thái trước khi đưa nó vào các thành phần Vue.js.
    getCount: (state) => state.count,
  },
};

export default example;
