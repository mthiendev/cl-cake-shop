import Vue from 'vue';

const cartModule = {
  state: {
    cartItems: [],
  },
  mutations: {
    addToCart(state, product) {
      const index = state.cartItems.findIndex(
        (cartItem) => product.id === cartItem.id,
      );
      if (index === -1) {
        state.cartItems.push({ ...product, quantity: 1 });
      } else {
        const quantity = state.cartItems[index].quantity + 1;
        Vue.set(state.cartItems, index, {
          ...state.cartItems[index],
          quantity,
        });
      }
    },
    removeCartItem(state, id) {
      const index = state.cartItems.findIndex((item) => item.id === id);
      state.cartItems.splice(index, 1);
    },
    updateCartItem(state, cartItem) {
      const index = state.cartItems.findIndex(
        (item) => item.id === cartItem.id,
      );
      Vue.set(state.cartItems, index, {
        ...state.cartItems[index],
        ...cartItem,
      });
    },
    orderCart(state) {
      state.cartItems = [];
    },
  },
  actions: {
    addToCart({ commit }, cartItem) {
      commit('addToCart', cartItem);
    },
    orderCart({ commit }) {
      commit('orderCart');
    },
    removeCartItem({ commit }, id) {
      commit('removeCartItem', id);
    },
    updateCartItem({ commit }, cartItem) {
      commit('updateCartItem', cartItem);
    },
  },
  getters: {
    getCartItems(state) {
      return state.cartItems;
    },
  },
};

export default cartModule;
