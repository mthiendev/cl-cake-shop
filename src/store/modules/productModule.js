import productListData from '@/assets/products.json';
import Vue from 'vue';

const productModule = {
  state: {
    products: productListData,
  },
  mutations: {
    updateProduct(state, updatedProduct) {
      const index = state.products.findIndex(
        (product) => product.id === updatedProduct.id,
      );
      const currentProduct = state.products[index];
      Vue.set(state.products, index, { ...currentProduct, ...updatedProduct });
    },
  },
  actions: {
    updateProduct({ commit }, product) {
      commit('updateProduct', product);
    },
  },
  getters: {
    getProductItems: (state) => {
      return state.products;
    },
    getProductById: (state) => (id) => {
      return state.products.find((product) => product.id === id);
    },
  },
};

export default productModule;
