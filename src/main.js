import Vue from 'vue';
import App from './App.vue';
import router from './router';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import store from '@/store/index.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import axiosPlugin from '@/plugins/axios-plugin';
import SweetAlertPlugin from '@/plugins/sweet-alert2';
import VeeValidate from 'vee-validate';

import '@/assets/main.css';
Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: 'veeFields',
  errorBagName: 'veeErrors',
});
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(axiosPlugin);
Vue.use(SweetAlertPlugin);

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
  store,
}).$mount('#app');
